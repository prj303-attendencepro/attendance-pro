// Import the Attendance smart contract
const Attendance = artifacts.require('Attendance');

contract('Attendance', function(accounts) {
    let attendanceInstance;


    beforeEach(async () => {
        attendanceInstance = await Attendance.new();
    });

    
    it('should deploy the contract properly', async () => {
        assert.ok(attendanceInstance.address);
    });

    
    it('should mark attendance as present and retrieve correct attendance status', async () => {
        const studentId = 12210033;
        const isPresent = true;

    
        await attendanceInstance.markAttendance(studentId, isPresent);

        
        const isPresentResult = await attendanceInstance.isStudentPresent(studentId);
        assert.equal(isPresentResult, isPresent, "Attendance not recorded correctly");

        
        const timestamp = await attendanceInstance.getAttendanceTimestamp(studentId);
        assert.ok(timestamp > 0, "Timestamp not retrieved correctly");
    });

    
    it('should mark attendance as absent and retrieve correct attendance status', async () => {
        const studentId = 12210024;
        const isPresent = false;

    
        await attendanceInstance.markAttendance(studentId, isPresent);

        
        const isPresentResult = await attendanceInstance.isStudentPresent(studentId);
        assert.equal(isPresentResult, isPresent, "Attendance not recorded correctly");

        
        const timestamp = await attendanceInstance.getAttendanceTimestamp(studentId);
        assert.ok(timestamp > 0, "Timestamp not retrieved correctly");
    });


    it('should initialize attendance records correctly', async () => {
        const studentId = 12210024;

        
        const initialRecord = await attendanceInstance.attendanceRecords(studentId);
        assert.equal(initialRecord.isPresent, false, "Attendance record not initialized correctly");
        assert.equal(initialRecord.timestamp, 0, "Timestamp not initialized correctly");
    });
});
