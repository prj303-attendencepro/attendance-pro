pragma solidity ^0.5.16;

contract Attendance {
    
    struct Student {
        bool isPresent;
        uint256 timestamp;
    }

    // Mapping to store student attendance records using their QR code ID
    mapping(uint256 => Student) public attendanceRecords;

    // Event to log attendance changes
    event AttendanceChanged(uint256 indexed studentId, bool isPresent, uint256 timestamp);

    // to mark attendance when a student scan
    function markAttendance(uint256 studentId, bool isPresent) public {
        attendanceRecords[studentId] = Student(isPresent, block.timestamp);
        emit AttendanceChanged(studentId, isPresent, block.timestamp);
    }

    // check if a student is present
    function isStudentPresent(uint256 studentId) public view returns (bool) {
        return attendanceRecords[studentId].isPresent;
    }

    // to get the timestamp of the last attendance
    function getAttendanceTimestamp(uint256 studentId) public view returns (uint256) {
        return attendanceRecords[studentId].timestamp;
    }
}
