const express = require('express');
const router = express.Router();
const { getAllSpecialization, create,update,del,getSpecialization } = require('../controllers/specializationController');




//create specialization
router.post("/create", create)
router.get("/specializations",getAllSpecialization )
router.get("/specialization",getSpecialization )
router.put('/update', update);
router.delete('/delete', del);



module.exports = router;
