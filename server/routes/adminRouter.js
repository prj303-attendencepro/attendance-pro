const express = require('express');
const router = express.Router();
const User = require('../models/User');
const { register,update,del, getUser,getAllUser } = require('../controllers/adminController');

//registration route
router.post('/register', register);
router.put('/update', update);
router.delete('/delete', del);
router.get('/getUser', getUser);
router.get('/getAllUser', getAllUser);
module.exports = router;
