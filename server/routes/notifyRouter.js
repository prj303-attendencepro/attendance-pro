const express = require('express');
const router = express.Router();
const MedicalReport = require('../models/medicalReport');

const Module = require('../models/Module');
const User = require('../models/User');
const multer = require('multer')
const path = require('path');
const fs = require('fs-extra');


const storage = multer.diskStorage({
  destination: async (req, file, cb) => {
    let path = 'uploads'

    fs.ensureDirSync(path);
    cb(null, path);
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
  }
});

const upload = multer({ storage: storage })



router.post('/MedicalReport', upload.single('photo'), async (req, res) => {

  const { student_id, module_id } = req.body;
  const photopath = req.file ? req.file.path : null;


  try {
    let medicalreport = await MedicalReport.create({
      student_id,
      module_id,
      photopath
    });




    if (!student_id || !module_id || !photopath) {
      return res.status(400).json({ error: 'Studentno, module_id, and photo are required' });
    }

    if (!photopath) {
      return res.status(400).json({ error: 'Failed to upload photo' });
    }

    await medicalreport.save();
    res.status(201).json({ message: 'Medical report uploaded successfully and module notified', medicalreport });
  } catch (error) {
    console.error('Error uploading medical report:', error);
    res.status(500).json({ error: 'Error uploading medical report' });
  }
});

router.get('/report', async (req, res) => {
  try {
    const medicalreport = await MedicalReport.find();
    res.json({ success: true, medicalreport });
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

router.get('/reports', async (req, res) => {
  try {
    const medicalreports = await MedicalReport.find();
    res.json({ success: true, medicalreports });
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

    
module.exports = router;

