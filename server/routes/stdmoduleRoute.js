const express = require('express');
const router = express.Router();
const { enrollInModule , getenrollInModule } = require('../controllers/stdmoduleController');

router.post('/enroll', enrollInModule);
router.get('/getE', getenrollInModule);

module.exports = router;
