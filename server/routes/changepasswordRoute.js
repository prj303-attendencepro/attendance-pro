const express = require('express');
const router = express.Router();
const User = require('../models/User');
const {update } = require('../controllers/changepasswordController');

router.put('/update', update);

module.exports = router;
