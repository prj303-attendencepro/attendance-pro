const express = require('express');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const User = require('../models/User');

const router = express.Router();

const config = {
  emailUser: '12210033.gcit@rub.edu.bt',
  emailPass: 'gfvfqkxqfwxiqnau',
};

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.emailUser,
    pass: config.emailPass,
  },
});

// Middleware to parse JSON bodies
router.use(bodyParser.json());

router.post('/reset', async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });

  if (!user) {
    return res.status(404).json({ message: 'No account with that email address exists.' });
  }

  const token = crypto.randomBytes(20).toString('hex');
  user.resetPasswordToken = token;
  user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
  await user.save();

  const mailOptions = {
    to: user.email,
    from: config.emailUser,
    subject: "Password Reset Request",
    text: `Dear ${user.email},

You are receiving this email because a request was made to reset the password for your account.

To reset your password, please click on the following link or paste it into your browser:

http://localhost:3000/reset-password?email=${encodeURIComponent(user.email)}

If you did not request this password reset, please ignore this email and your password will remain unchanged.

Thank you,
Attendpro Support Team`,
   
};

  transporter.sendMail(mailOptions, (err, response) => {
    if (err) {
      console.error('There was an error: ', err);
      return res.status(500).json({ message: 'Failed to send recovery email.' });
    } else {
      res.send("Okay");
    }
  });
});

router.get('/resetP', async (req, res) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() },
  });

  if (!user) {
    return res.send('Password reset token is invalid or has expired.');
  }

  res.sendFile(__dirname + 'view', 'resetpassword.js');
});



module.exports = router;
