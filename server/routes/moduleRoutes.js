const express = require('express');
const router = express.Router();
const Module = require('../models/Module');
const { getAllModule, add, update, del, getModule } = require('../controllers/moduleController');
const multer = require('multer')
const path = require('path');
const fs = require('fs-extra');

const storage = multer.diskStorage({
    destination: async (req, file, cb) => {
        let path = 'uploads'

        fs.ensureDirSync(path);
        cb(null, path);
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage })


//add module
router.post("/add", upload.single('photo'), add)
router.get("/modules", getAllModule)
router.post('/update', upload.single('photo'), update);
router.get("/getmodule", getModule)


router.delete('/delete', del);





module.exports = router;
