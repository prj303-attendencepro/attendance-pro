const mongoose = require('mongoose');

const moduleSchema = new mongoose.Schema({
    modulename: { type: String, required: true },
    modulecode: { type: String, required: true, unique: true },
    photopath: { type: String, required: true },
    numofc: { type:String, required: true },
    students: {type: Array}
});

const Module = mongoose.model('Module', moduleSchema);

module.exports = Module;
