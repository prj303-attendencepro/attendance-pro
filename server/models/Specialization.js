const mongoose = require('mongoose');

const specializationSchema = new mongoose.Schema({
    specializationname: { type: String, required: true },
    specializationId: { type: String, required: true },
    numofc: { type: Number, required: true }
});

const Specialization = mongoose.model('Specialization', specializationSchema);

module.exports = Specialization;
