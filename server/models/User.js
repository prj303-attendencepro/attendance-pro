const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, enum: ['student', 'teacher', 'admin'], required: true },
    name: { type: String, required: true },
    studentno: {
        type: Number,
        unique: function () {
            return this.role === 'student';
        }
    },
    phoneno: {
        type: Number,
        unique: function () {
            return this.role === 'teacher' || this.role === 'student';
        }
    },
    employeeid: {
        type: Number,
        unique: function () {
            return this.role === 'teacher';
        },
        sparse: true // Allows multiple documents to have null for employeeid
    },
    gender: {
        type: String,
        enum: ['male', 'female'],
        required: true
    },
    notifications: {
        type: Array,
    },
    year: {
        type: Number,
        validate: {
            validator: function (value) {
                return this.role !== 'student' || (value >= 1 && value <= 4);
            },
            message: 'Year is required and must be 1, 2, 3, or 4 if role is student.'
        }
    },
    resetPasswordToken: { type: String },
    resetPasswordExpires: { type: Date },

    
});


const User = mongoose.model('User', userSchema);

module.exports = User;
