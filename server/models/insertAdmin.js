require('dotenv').config({ path: './config.env' });
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const User = require('../models/User'); // Ensure the correct path to your User model

const createAdminUser = async () => {
    console.log("CREATE ADMIN USER")

    try {
        const dbURI = process.env.DATABASE;
        if (!dbURI) {
            throw new Error('Database connection string is not defined in environment variables');
        }

        // Connect to MongoDB Atlas
        await mongoose.connect(dbURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        // Check if admin already exists
        const existingAdmin = await User.findOne({ email: 'admin@example.com' });
        if (existingAdmin) {
            console.log('Admin user already exists');
            return;
        }

        // Create new admin user
        const password ='gcitadminpassword123';
        const newAdmin = new User({
            name: 'Admin',
            email: 'admin@example.com',
            gender: 'male',
            phoneno: 17741335,
            password: password,
            role: 'admin'
        });

        await newAdmin.save();
        console.log('Admin user created successfully');
    } catch (err) {
        console.error('Error inserting admin user:', err);
    } finally {
        // Only close the connection if this script runs separately from the server
        if (process.env.NODE_ENV !== 'development') {
            mongoose.connection.close();
        }
    }
};

module.exports = createAdminUser;
