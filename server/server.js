const express = require('express');
const dotenv = require("dotenv");
const mongoose = require('mongoose');

const authRouter = require('./routes/authRoutes');
dotenv.config({ path : "./config.env"})
const moduleRouter = require('./routes/moduleRoutes');
const specializationRouter = require('./routes/specializationRoute');
const changepasswordRouter = require('./routes/changepasswordRoute');
const notificationRoutes = require('./routes/notifyRouter');

const stdmoduleRouter = require('./routes/stdmoduleRoute');
const path = require('path')

const adminRouter = require('./routes/adminRouter');

const bodyParser = require('body-parser');
const cors = require("cors");
const cookieParser = require('cookie-parser');
const createAdminUser = require('./models/insertAdmin');
const forgotRoutes = require('./routes/forgotpasswordRoute');

const app = express()
const port = 3001

const corsOptions = {
  origin: 'http://localhost:3000', 
  credentials: true 
};

app.use(cors(corsOptions));

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/routes/admin', adminRouter);
app.use('/routes/auth/', authRouter);
app.use('/routes/module', moduleRouter);
app.use('/routes/course', specializationRouter);
app.use('/routes/password', changepasswordRouter);
app.use('/notifications', notificationRoutes);
app.use('/routes/enrolledm', stdmoduleRouter);
app.use('/routes/notify', notificationRoutes);
app.use('/routes',forgotRoutes );


createAdminUser();

mongoose.connect('mongodb://localhost:27017/server', { useNewUrlParser: true, useUnifiedTopology: true });
const ds = mongoose.connection;
ds.on("error", console.error.bind(console,'MongoDB connection error'));

ds.once('open', () => {
  console.log('Connected to MongoDB');
});



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})