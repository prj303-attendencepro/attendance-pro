const User = require('../models/User');
const Module = require('../models/Module');

async function enrollInModule(req, res) {
  const {module_id,user_id } = req.body;
  try {
    

    if (!user_id) {
      return res.status(400).json({ message: 'User not found' });
    }

    console.log(req.body)
    
    const [student, module] = await Promise.all([
      User.findById(user_id),
      Module.findById(module_id),
    ]);

    if (!student) {
      return res.status(404).json({ message: 'Student not found' });
    }

    if (!module) {
      return res.status(404).json({ message: 'Module not found' });
    }

    
    if (!Array.isArray(student.modules)) {
      student.modules = [];
    }

    
    if (module.students.includes(user_id)) {
      return res.status(400).json({ message: 'Student is already enrolled in this module' });
    }

    
    module.students.push(user_id);
    await module.save();

    res.status(200).json({ message: 'Student enrolled successfully' });
  } catch (error) {
    console.error('Error enrolling student:', error); 
    res.status(500).json({ message: 'Internal server error' });
  }
}

async function getenrollInModule(req, res) {
  try {
      const modules = await Module.find();
      const students = modules.reduce((acc, module) => acc.concat(module.students), []);
      res.json({ success: true, students });
  } catch (err) {
      console.error(err);
      res.status(500).json({ success: false, message: "Internal server error" });
  }
}




module.exports = { enrollInModule ,getenrollInModule };

