const User = require('../models/User');
const jwt = require("jsonwebtoken");
const AppError = require("../utils/appError");

require('dotenv').config({ path: './config.env' });

const signToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    });
};

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id);
    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };
    res.cookie("jwt", token, cookieOptions);
    res.status(statusCode).json({
        status: "success",
        token,
        data: {
            user
        }
    });
};

exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return next(new AppError("Please provide an email and password!", 400));
        }

        const user = await User.findOne({ email }).select('+password');
        if (!user) {
            return next(new AppError("Incorrect email or password", 401));
        }

        // Compare plain text passwords
        if (password !== user.password) {
            return next(new AppError("Incorrect email or password", 401));
        }

        createSendToken(user, 200, res);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.logout = (req, res) => {
    res.clearCookie("jwt");
    res.status(200).json({ status: "success" });
};


