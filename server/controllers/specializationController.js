const Specialization = require('../models/Specialization');

async function create(req, res) {
    const { specializationname, specializationId, numofc } = req.body;
    console.log(req.file)

    try {
        let specialization = await Specialization.create({
            specializationname,
            specializationId,
            numofc
        });

        if (!specialization) {
            return res.status(401).json({ success: false, message: "Invalid Specialization Name" });
        } else {
            res.json({ success: true, message: "Create successful" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function getSpecialization(req, res) {
    try {
        const specialization = await Specialization.find();
        res.json({ success: true, specialization });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function getAllSpecialization(req, res) {
    try {
        const specializations = await Specialization.find();
        res.json({ success: true, specializations });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function update(req, res) {
    console.log('testx', req.body)
    const { specializationname, id, specializationId, numofc } = req.body;
    try {
        let specialization = await Specialization.findOneAndUpdate(
            { _id: id },
            {
                $set: {
                    specializationname,
                    specializationId,
                    numofc
                }
            },
            { new: true } 
        );

        if (!specialization) {
            return res.status(401).json({ success: false, message: "Specialization not found!" });
        } else {
            res.json({ success: true, message: "Updated successful" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

async function del(req, res) {
    const { id } = req.body;
    console.log("test", req.body)

    try {
        const specialization = await Specialization.findById(id);
        if (!specialization) {
            return res.status(400).json({ success: false, message: "Specialization not found!" });
        }

        await Specialization.findOneAndDelete({ _id: id });

        res.json({ success: true, message: "Deleted successful" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

module.exports = {
    create,
    getSpecialization,
    getAllSpecialization,
    update,
    del
};
