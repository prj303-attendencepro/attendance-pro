const Module = require('../models/Module');

async function add(req, res) {
    const { modulename, modulecode, numofc } = req.body;
    const photopath = req.file ? req.file.path : null;

    try {
        let module = await Module.create({
            modulename,
            modulecode,
            numofc,
            photopath
        });

        if (!module) {
            return res.status(401).json({ success: false, message: "Invalid module Name or module Code " });
        } else {
            res.json({ success: true, message: "Add successful" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function getModule(req, res) {
    try {
        const module = await Module.find();
        res.json({ success: true, module });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function getAllModule(req, res) {
    try {
        const modules = await Module.find();
        res.json({ success: true, modules });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function update(req, res) {
    const { modulename, modulecode, id, numofc } = req.body;
    const photopath = req.file ? req.file.path : null;

    try {
        let module;
        if (photopath) {
            module = await Module.findOneAndUpdate(
                { _id: id },
                {
                    $set: {
                        modulename,
                        modulecode,
                        numofc,
                        photopath
                    }
                },
                { new: true }
            );
        } else {
            module = await Module.findOneAndUpdate(
                { _id: id },
                {
                    $set: {
                        modulename,
                        modulecode,
                        numofc,
                    }
                },
                { new: true }
            );
        }

        if (!module) {
            return res.status(400).json({ success: false, message: "Module not found!" });
        } else {
            res.json({ success: true, message: "Updated successfully" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

async function del(req, res) {
    const { id } = req.body;

    try {
        const module = await Module.findById(id);
        if (!module) {
            return res.status(400).json({ success: false, message: "Module not found!" });
        }

        await Module.findOneAndDelete({ _id: id });

        res.json({ success: true, message: "Deleted successfully" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

module.exports = {
    add,
    getModule,
    getAllModule,
    update,
    del
};
