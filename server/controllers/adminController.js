const User = require('../models/User');

async function register(req, res) {
    const { email, password, role, studentno, phoneno, employeeid, gender, name, year, clas } = req.body;

    try {
        console.log("employeeid", employeeid);
        let user = await User.create({
            email,
            name,
            password,
            role,
            studentno,
            phoneno,
            employeeid,
            gender,
            clas,
            year
        });

        if (!user) {
            return res.status(401).json({ success: false, message: "Invalid credentials" });
        } else {
            res.json({ success: true, message: "Registration successful" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

async function update(req, res) {
    const { email, password, role, studentno, phoneno, employeeid, gender, name, year, clas, id } = req.body;

    try {
        let user = await User.findOneAndUpdate(
            { _id: id },
            {
                $set: {
                    email,
                    name,
                    password,
                    role,
                    studentno,
                    phoneno,
                    employeeid,
                    gender,
                    clas,
                    year
                }
            },
            { new: true } 
        );

        if (!user) {
            return res.status(404).json({ success: false, message: "User not found!" });
        } else {
            res.json({ success: true, message: "Update successful" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

async function getUser(req, res) {
    try {
        const user = await User.find();
        res.json({ success: true, user });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function getAllUser(req, res) {
    try {
        const users = await User.find();
        res.json({ success: true, users });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
}

async function del(req, res) {
    const { id } = req.body;

    try {
        const user = await User.findById(id);
        if (!user) {
            return res.status(404).json({ success: false, message: "User not found!" });
        }

        await User.findOneAndDelete({ _id: id });

        res.json({ success: true, message: "Deletion successful" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

module.exports = {
    register,
    update,
    del,
    getUser,
    getAllUser
};
