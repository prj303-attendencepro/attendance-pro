const User = require('../models/User');

async function update(req, res) {
    const { currentpassword, newpassword, confirmedpassword, id } = req.body;

    try {
        let user = await User.findById(id);

        if (!user) {
            return res.status(404).json({ success: false, message: "User not found!" });
        }

        
        if (currentpassword !== user.password) {
            return res.status(401).json({ success: false, message: "Current password is incorrect!" });
        }

        
        if (newpassword !== confirmedpassword) {
            return res.status(400).json({ success: false, message: "New password and confirmed password do not match!" });
        }

        user.password = newpassword;

        await user.save();

        
        res.json({ success: true, message: "Password updated successfully" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: err.message });
    }
}

module.exports ={ update};
