module.exports = {
  networks: {
    development: {
      host: "host.docker.internal",     // Localhost (default: none)
      port: 7545,            // Port number of your Ethereum client
      network_id: "*",   // Network ID of your Ethereum client
    },
    // Uncomment and configure other networks as needed
    // ropsten: {
    //   provider: () => new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/YOUR-PROJECT-ID`),
    //   network_id: 3,
    //   gas: 5500000,
    //   confirmations: 2,
    //   timeoutBlocks: 200,
    //   skipDryRun: true
    // },
    // private: {
    //   provider: () => new HDWalletProvider(mnemonic, `https://network.io`),
    //   network_id: 2111,
    //   production: true
    // }
  },

  compilers: {
    solc: {
      version: "0.5.16",  // Solidity compiler version
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        },
      }
    }
  },

  mocha: {
    // timeout: 100000
  },

  db: {
    enabled: false
  }
};
